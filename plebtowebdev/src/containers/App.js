import React, { Component } from 'react';
import TimeClock from '../components/timer';
import logo from '../images/logo.svg'
import './App.css';


class App extends Component {

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Hurricane Irma Relief Information</h2>
        </div>
        <TimeClock className="App-intro" />
        <p className="App-intro">
          Christine, Chill Out. It's not that bad.
        </p>
      </div>
    );
  }
}

export default App;
