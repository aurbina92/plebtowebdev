import React, { Component } from 'react';
import './components.css';

class TimeClock extends Component{
    constructor(props) {
        super(props);
        this.state = {date: new Date()};
    }
    

    componentDidMount(){
        this.timerID = setInterval(
            () => this.tick(), 1000 );
    }
    componentWillUnmount(){
        clearInterval(this.timerID);
    }

    tick(){
        this.setState({
            date: new Date()
        });
    }

    render() {
        return (
            <div className='timer-style'>
                <h2>
                    {this.state.date.toLocaleTimeString()}
                </h2>
            </div>
        );
    }
}

export default TimeClock;